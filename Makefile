CXX := c++
CFLAGS += -pthread
OBJ := pgrep.o
PROG := pgrep

$(PROG) : $(OBJ)
	$(CXX) -o $@ $(CFLAGS) $^

$(OBJ) : $(OBJ:.o=.cpp)
	$(CXX) -c -o $@ $(CFLAGS) $^

clean :
	rm -f $(PROG) $(OBJ)

.PHONY: clean
