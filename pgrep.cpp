#include <iostream>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <memory>
#include <string>
#include <dirent.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/types.h>
#include <regex>

std::vector<pthread_t> thread_ids;

sem_t semaphore; // garante que o numero de threads em execucao nunca excede o maximo
pthread_mutex_t output_mutex; // garante que so' uma thread imprime em stdout por vez

void print_matching_line(std::string regex_str, std::string filename)
{
	std::vector<int> num_lines;
	std::ifstream file{filename};
	std::regex regex{regex_str, std::regex::extended}; // regex POSIX estendida
	int line_num = 0;
	while (file.good()) {
		std::string line;
		std::getline(file, line);
		std::smatch match;

		if (std::regex_search(line, match, regex)) {
			num_lines.push_back(line_num);			
		}
		++line_num;
	}

	// secao critica
	pthread_mutex_lock(&output_mutex);
	for(int i : num_lines){
		std::cout << filename << ": " << i << std::endl;
	}
	pthread_mutex_unlock(&output_mutex);
}

struct arg_wrapper {
	arg_wrapper(std::string regex_str_, std::string filename_)
		: regex_str{regex_str_}, filename{filename_} {}

	std::string regex_str;
	std::string filename;
};

void *callback(void *args)
{
	auto *wrapped_args = static_cast<arg_wrapper*>(args);
	// novamente, movemos as strings porque nao as usaremos mais.
	print_matching_line(std::move(wrapped_args->regex_str), std::move(wrapped_args->filename));
	delete wrapped_args;
	sem_post(&semaphore);

	return NULL;
}

// tenta criar uma thread, se nao atingimos o numero maximo ainda.
void make_thread(std::string regex_str, std::string filename)
{
	// movemos as strings (em vez de copiar) porque nao vamos mais usa-las
	arg_wrapper *args = new arg_wrapper(std::move(regex_str), std::move(filename));

	pthread_t thread_id;

	sem_wait(&semaphore);
	//if (pthread_create(&thread_id, NULL, callback, args.get()) != 0) {
	if (pthread_create(&thread_id, NULL, callback, args)) {
		std::cerr << "failed to create thread!" << std::endl;
		return;
	}
	thread_ids.push_back(thread_id);
}

// itera sobre o diretorio dado e seus sub-diretorios
void traverse_dir_tree(std::string regex_str, std::string dirname, std::string path_prefix)
{
	std::string dirpath{path_prefix + dirname + "/"};
	DIR *dir = opendir(dirpath.c_str());
	if (dir == NULL) {
		perror(dirpath.c_str());
		return;
	}

	struct dirent *dirent = readdir(dir);
	for (; dirent != NULL; dirent = readdir(dir)) {
		std::string name{dirent->d_name};
		if (dirent->d_type == DT_DIR
				&& name != "."
				&& name != "..") {
			traverse_dir_tree(regex_str, name, dirpath);
		} else if (dirent->d_type == DT_REG) {
			make_thread(regex_str, dirpath + name);
		}
	}

	closedir(dir);
}

int main(int argc, char **argv)
{
	if (argc != 4) {
		std::cerr << "uso: " << argv[0] << " MAX_THREADS REGEX CAMINHO_DIRETORIO" << std::endl;
		return -1;
	}
	int max_threads = atoi(argv[1]);
	char *regex_str = argv[2];
	char *dirpath = argv[3];

	if (sem_init(&semaphore, 0, max_threads) != 0) {
		std::cerr << "inicializacao do semaforo falhou." << std::endl;
		return -1;
	}
	if (pthread_mutex_init(&output_mutex, NULL) != 0) {
		std::cerr << "inicializacao da output_mutex falhou." << std::endl;
		return -1;
	}
	
	// remove a '/' no final do path, caso haja
	size_t dirlen = strlen(dirpath);
	if (dirpath[dirlen - 1] == '/') {
		dirpath[dirlen - 1] = '\0';
	}

	traverse_dir_tree(regex_str, dirpath, "");

	for (pthread_t thread_id : thread_ids) {
		if (pthread_join(thread_id, NULL) != 0) {
			std::cerr << "erro em pthread_join" << std::endl;
		}
	}

	pthread_mutex_destroy(&output_mutex);
	sem_destroy(&semaphore);
	return 0;
}
